%global debug_package %{nil}

Name:           gum
Version:        0.14.1
Release:        2%{?dist}
Summary:        A tool for glamorous shell scripts

License:        MIT
URL:            https://github.com/charmbracelet/%{name}
Source:         https://github.com/charmbracelet/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  go

%description
A tool for glamorous shell scripts. Leverage the power of Bubbles and Lip Gloss
in your scripts and aliases without writing any Go code!

%prep
%autosetup -n %{name}-%{version}

%build
go build -ldflags "-w" -o %{name}

%install
install -Dpm 0755 %{name} -t %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 0.14.1-2
- refactor: Make RPM specs cleaner

* Sat Jun 08 2024 Andrey Brusnik <dev@shdwchn.io> - 0.14.1-1
- Initial commit
