%global debug_package %{nil}

Name:           nitrokey-udev-rules
Version:        1.1.0
Release:        1%{?dist}
Summary:        udev rules for Nitrokey devices

License:        CC0-1.0
URL:            https://github.com/Nitrokey/%{name}
Source:         https://github.com/Nitrokey/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  systemd-devel

BuildArch:	noarch

%description
%{summary}.

%prep
%autosetup -n %{name}-%{version}

%install
install -Dpm 0644 41-nitrokey.rules -t %{buildroot}%{_udevrulesdir}/

%files
%doc README.md CHANGELOG.md
%license LICENSE
%{_udevrulesdir}/41-nitrokey.rules

%changelog
* Mon Feb 03 2025 Andrey Brusnik <dev@shdwchn.io> - 1.1.0-1
- feat: Add nitrokey-udev-rules package
