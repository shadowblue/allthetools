%global debug_package %{nil}

Name:           chezmoi
Version:        2.60.0
Release:        1%{?dist}
Summary:        Manage your dotfiles across multiple diverse machines, securely.

License:        MIT
URL:            https://github.com/twpayne/%{name}
Source:         https://github.com/twpayne/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  go

%description
chezmoi helps you manage your personal configuration files (dotfiles, like ~/.gitconfig) across multiple machines.

chezmoi provides many features beyond symlinking or using a bare git repo including:
templates (to handle small differences between machines), password manager support (to store your secrets securely),
importing files from archives (great for shell and editor plugins), full file encryption (using gpg or age),
and running scripts (to handle everything else).

%prep
%autosetup -n %{name}-%{version}

%build
go build -ldflags "-w -X main.version=%{version} \
                      -X main.date=$(date -d "@${SOURCE_DATE_EPOCH}" +%Y-%m-%d)" \
                      -o %{name}
mkdir generated_completions
./%{name} completion bash -o generated_completions/%{name}
./%{name} completion fish -o generated_completions/%{name}.fish
./%{name} completion zsh  -o generated_completions/_%{name}

%install
install -Dpm 0755 %{name} -t %{buildroot}%{_bindir}/
install -Dpm 0644 generated_completions/%{name} -t %{buildroot}/%{bash_completions_dir}/
install -Dpm 0644 generated_completions/%{name}.fish -t %{buildroot}/%{fish_completions_dir}/
install -Dpm 0644 generated_completions/_%{name}     -t %{buildroot}/%{zsh_completions_dir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{bash_completions_dir}/%{name}
%{fish_completions_dir}/%{name}.fish
%{zsh_completions_dir}/_%{name}

%changelog
* Fri Feb 28 2025 Andrey Brusnik <dev@shdwchn.io> - 2.60.0-1
- chore(chezmoi): Bump to 2.60.0

* Sun Feb 09 2025 Andrey Brusnik <dev@shdwchn.io> - 2.59.1-1
- chore(chezmoi): Bump to 2.59.1

* Wed Feb 05 2025 Andrey Brusnik <dev@shdwchn.io> - 2.59.0-1
- chore(chezmoi): Bump to 2.59.0

* Tue Jan 14 2025 Andrey Brusnik <dev@shdwchn.io> - 2.58.0-1
- chore(chezmoi): Bump to 2.58.0

* Thu Dec 19 2024 Andrey Brusnik <dev@shdwchn.io> - 2.56.0-1
- chore(chezmoi): Bump to 2.56.0

* Thu Dec 12 2024 Andrey Brusnik <dev@shdwchn.io> - 2.55.0-1
- chore(chezmoi): Bump to 2.55.0

* Tue Nov 26 2024 Andrey Brusnik <dev@shdwchn.io> - 2.54.0-1
- chore(chezmoi): Bump to 2.54.0

* Wed Oct 30 2024 Andrey Brusnik <dev@shdwchn.io> - 2.53.1-1
- chore(chezmoi): Bump to 2.53.1

* Sat Oct 19 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.4-1
- chore(chezmoi): Bump to 2.52.4

* Mon Sep 30 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.3-1
- chore(chezmoi): Bump to 2.52.3

* Wed Sep 11 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.2-1
- chore(chezmoi): Bump to 2.52.2

* Sat Sep 07 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.1-2
- fix(chezmoi): Fix bash completions path

* Sun Aug 18 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.1-1
- chore(chezmoi): Bump to 2.52.1

* Thu Aug 08 2024 Andrey Brusnik <dev@shdwchn.io> - 2.52.0-1
- chore(chezmoi): Bump to 2.52.0

* Wed Jul 03 2024 Andrey Brusnik <dev@shdwchn.io> - 2.50.0-1
- chore(chezmoi): Bump to 2.50.0

* Wed Jun 26 2024 Andrey Brusnik <dev@shdwchn.io> - 2.49.1-1
- chore(chezmoi): Bump to 2.49.1

* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 2.49.0-3
- feat(chezmoi): Install completions

* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 2.49.0-2
- refactor: Make RPM specs cleaner

* Tue Jun 11 2024 Andrey Brusnik <dev@shdwchn.io> - 2.49.0-1
- feat: Added chezmoi package
