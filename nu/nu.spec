%global debug_package %{nil}

%define reponame nushell

Name:           nu
Version:        0.102.0
Release:        1%{?dist}
Summary:        A new type of shell

License:        MIT
URL:            https://github.com/nushell/%{reponame}
Source:         https://github.com/nushell/%{reponame}/archive/refs/tags/%{version}.tar.gz

BuildRequires:  cargo
BuildRequires:  rust
BuildRequires:  openssl-devel

%if 0%{?fedora} >= 41
BuildRequires:  openssl-devel-engine
%endif

%description
Nu takes cues from a lot of familiar territory: traditional shells like bash,
object based shells like PowerShell, gradually typed languages like TypeScript,
functional programming, systems programming, and more.
But rather than trying to be a jack of all trades, Nu focuses its energy on doing a few things well:
- Being a flexible cross-platform shell with a modern feel
- Solving problems as a modern programming language that works with the structure of your data
- Giving clear error messages and clean IDE support

%prep
%autosetup -n %{reponame}-%{version}

%build
cargo build --release --locked --workspace

%install
install -Dpm 0755 target/release/%{name} -t %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Wed Feb 05 2025 Andrey Brusnik <dev@shdwchn.io> - 0.102.0-1
- chore(nu): Bump to 0.102.0

* Wed Dec 25 2024 Andrey Brusnik <dev@shdwchn.io> - 0.101.0-1
- chore(nu): Bump to 0.101.0

* Tue Nov 26 2024 Andrey Brusnik <dev@shdwchn.io> - 0.100.0-1
- chore(nu): Bump to 0.100.0

* Wed Oct 30 2024 Andrey Brusnik <dev@shdwchn.io> - 0.99.1-1
- chore(nu): Bump to 0.99.1

* Sat Oct 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.99.0-1
- chore(nu): Bump to 0.99.0

* Thu Sep 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.98.0-1
- chore(nu): Bump to 0.98.0

* Fri Aug 23 2024 Andrey Brusnik <dev@shdwchn.io> - 0.97.1-2
- fix(nu): Require deprecated in F41+ OpenSSL ENGINE API to build pkg

* Thu Aug 22 2024 Andrey Brusnik <dev@shdwchn.io> - 0.97.1-1
- chore(nu): Bump to 0.97.1

* Sun Aug 04 2024 Andrey Brusnik <dev@shdwchn.io> - 0.96.1-1
- chore(nu): Bump to 0.96.1

* Wed Jun 26 2024 Andrey Brusnik <dev@shdwchn.io> - 0.95.0-1
- chore(nu): Bump to 0.95.0

* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 0.94.2-2
- refactor: Make RPM specs cleaner

* Tue Jun 11 2024 Andrey Brusnik <dev@shdwchn.io> - 0.94.2-1
- feat: Added nu package
