%global debug_package %{nil}

Name:           rclone
Version:        1.69.1
Release:        1%{?dist}
Summary:        "rsync for cloud storage"

License:        MIT
URL:            https://rclone.org/
Source:         https://github.com/%{name}/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  go

%description
Rclone ("rsync for cloud storage") is a command-line program to sync files and
directories to and from different cloud storage providers.

%prep
%autosetup -n %{name}-%{version}

%build
go build -ldflags "-w -X github.com/rclone/rclone/fs.Version=%{version}" \
                      -o %{name}
mkdir generated_completions
./%{name} completion bash - > generated_completions/%{name}
./%{name} completion fish - > generated_completions/%{name}.fish
./%{name} completion zsh  - > generated_completions/_%{name}

%install
install -Dpm 0755 %{name} -t %{buildroot}%{_bindir}/
ln -rs %{buildroot}%{_bindir}/rclone %{buildroot}%{_bindir}/mount.%{name}
ln -rs %{buildroot}%{_bindir}/rclone %{buildroot}%{_bindir}/rclonefs
install -Dpm 0644 %{name}.1 %{buildroot}%{_mandir}/man1/%{name}.1
install -Dpm 0644 generated_completions/%{name}      -t %{buildroot}%{bash_completions_dir}/
install -Dpm 0644 generated_completions/%{name}.fish -t %{buildroot}%{fish_completions_dir}/
install -Dpm 0644 generated_completions/_%{name}     -t %{buildroot}%{zsh_completions_dir}/

%files
%license COPYING
%doc MANUAL.md README.md
%{_bindir}/%{name}
%{_bindir}/mount.%{name}
%{_bindir}/rclonefs
%{_mandir}/man1/%{name}.1*
%{bash_completions_dir}/%{name}
%{fish_completions_dir}/%{name}.fish
%{zsh_completions_dir}/_%{name}

%changelog
* Fri Feb 28 2025 Andrey Brusnik <dev@shdwchn.io> - 1.69.1-1
- chore(rclone): Bump to 1.69.1

* Tue Jan 14 2025 Andrey Brusnik <dev@shdwchn.io> - 1.69.0-1
- chore(rclone): Bump to 1.69.0

* Tue Nov 26 2024 Andrey Brusnik <dev@shdwchn.io> - 1.68.2-1
- chore(rclone): Bump to 1.68.2

* Wed Sep 25 2024 Andrey Brusnik <dev@shdwchn.io> - 1.68.1-1
- chore(rclone): Bump to 1.68.1

* Sat Sep 07 2024 Andrey Brusnik <dev@shdwchn.io> - 1.67.0-2
- fix(rclone): Fix bash completions path

* Fri Jun 28 2024 Andrey Brusnik <dev@shdwchn.io> - 1.67.0-1
- feat: Add rclone package
