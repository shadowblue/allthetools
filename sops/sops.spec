%global debug_package %{nil}

Name:           sops
Version:        3.9.4
Release:        1%{?dist}
Summary:        Simple and flexible tool for managing secrets

License:        MPL-2.0
URL:            https://github.com/getsops/%{name}
Source:         https://github.com/getsops/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git-core
BuildRequires:  go

%description
SOPS is an editor of encrypted files that supports
YAML, JSON, ENV, INI and BINARY formats and
encrypts with AWS KMS, GCP KMS, Azure Key Vault, age, and PGP.

%prep
%autosetup -n %{name}-%{version}
go mod download -x

%build
go build -buildmode=pie -ldflags "-w" -o %{name} ./cmd/sops

%install
install -Dpm 0755 %{name} -t %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc README.rst
%{_bindir}/%{name}

%changelog
* Thu Mar 06 2025 Andrey Brusnik <dev@shdwchn.io> - 3.9.4-1
- feat: Add sops package
