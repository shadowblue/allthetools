%global debug_package %{nil}

Name:           eza
Version:        0.20.23
Release:        1%{?dist}
Summary:        A modern replacement for ls

License:        EUPL-1.2
URL:            https://github.com/eza-community/%{name}
Source:         https://github.com/eza-community/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  cargo
BuildRequires:  rust
BuildRequires:  pandoc

%description
eza is a modern, maintained replacement for the venerable file-listing
command-line program ls that ships with Unix and Linux operating systems,
giving it more features and better defaults.
It uses colours to distinguish file types and metadata.
It knows about symlinks, extended attributes, and Git.
And it’s small, fast, and just one single binary.

By deliberately making some decisions differently,
eza attempts to be a more featureful, more user-friendly version of ls.

%prep
%autosetup -n %{name}-%{version}

%build
cargo build --release --locked

%install
install -Dpm 0755 target/release/%{name} -t %{buildroot}%{_bindir}/
# Man
mkdir target/man
for page in eza.1 eza_colors.5 eza_colors-explanation.5; do
    sed "s/\$version/v%{version}/g" "man/${page}.md" | pandoc --standalone -f markdown -t man > "target/man/${page}"
done;
install -Dpm 0644 target/man/eza.1 -t %{buildroot}/%{_mandir}/man1/
install -Dpm 0644 target/man/eza_colors.5 -t %{buildroot}/%{_mandir}/man5/
install -Dpm 0644 target/man/eza_colors-explanation.5 -t %{buildroot}/%{_mandir}/man5/
# Shell completions
install -Dpm 0644 completions/bash/%{name} -t %{buildroot}/%{bash_completions_dir}
install -Dpm 0644 completions/fish/%{name}.fish -t %{buildroot}/%{fish_completions_dir}
install -Dpm 0644 completions/zsh/_%{name} -t %{buildroot}/%{zsh_completions_dir}

%files
%license LICENSE.txt
%doc CHANGELOG.md README.md
%{_bindir}/%{name}
%{_mandir}/man1/eza.1*
%{_mandir}/man5/eza_colors*
%{bash_completions_dir}/%{name}
%{fish_completions_dir}/%{name}.fish
%{zsh_completions_dir}/_%{name}

%changelog
* Fri Feb 28 2025 Andrey Brusnik <dev@shdwchn.io> - 0.20.23-1
- chore(eza): Bump to 0.20.23

* Sun Feb 09 2025 Andrey Brusnik <dev@shdwchn.io> - 0.20.20-1
- chore(eza): Bump to 0.20.20

* Wed Feb 05 2025 Andrey Brusnik <dev@shdwchn.io> - 0.20.19-1
- chore(eza): Bump to 0.20.19

* Tue Jan 14 2025 Andrey Brusnik <dev@shdwchn.io> - 0.20.16-1
- chore(eza): Bump to 0.20.16

* Fri Dec 27 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.14-1
- chore(eza): Bump to 0.20.14

* Thu Dec 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.13-1
- chore(eza): Bump to 0.20.13

* Thu Dec 12 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.12-1
- chore(eza): Bump to 0.20.12

* Fri Nov 29 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.10-1
- chore(eza): Bump to 0.20.10

* Tue Nov 26 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.9-1
- chore(eza): Bump to 0.20.9

* Mon Nov 04 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.6-1
- chore(eza): Bump to 0.20.6

* Wed Oct 30 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.5-1
- chore(eza): Bump to 0.20.5

* Sat Oct 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.4-1
- chore(eza): Bump to 0.20.4

* Thu Sep 26 2024 Andrey Brusnik <dev@shdwchn.io> - 0.20.0-1
- chore(eza): Bump to 0.20.0
- chore(eza): Change license from MIT to EUPL-1.2

* Thu Sep 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.19.4-1
- chore(eza): Bump to 0.19.4

* Thu Sep 12 2024 Andrey Brusnik <dev@shdwchn.io> - 0.19.3-1
- chore(eza): Bump to 0.19.3

* Fri Sep 06 2024 Andrey Brusnik <dev@shdwchn.io> - 0.19.2-1
- chore(eza): Bump to 0.19.2

* Wed Aug 28 2024 Andrey Brusnik <dev@shdwchn.io> - 0.19.1-1
- chore(eza): Bump to 0.19.1

* Thu Aug 08 2024 Andrey Brusnik <dev@shdwchn.io> - 0.19.0-1
- chore(eza): Bump to 0.19.0

* Sun Aug 04 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.24-1
- chore(eza): Bump to 0.18.24

* Sat Jul 20 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.22-1
- chore(eza): Bump to 0.18.22

* Tue Jul 02 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.21-1
- chore(eza): Bump to 0.18.21

* Thu Jun 27 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.20-1
- chore(eza): Bump to 0.18.20

* Thu Jun 20 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.19-1
- chore(eza): Bump to 0.18.19

* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.18-2
- refactor: Make RPM specs cleaner

* Thu Jun 13 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.18-1
- chore(eza): Bump to 0.18.18

* Sat Jun 08 2024 Andrey Brusnik <dev@shdwchn.io> - 0.18.17-1
- Initial commit
