%global debug_package %{nil}
%global commit 3b5c384ac79af16b209ee76bceacdb7f92a32e7c
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           vk_hdr_layer
Version:        20240723git.%{shortcommit}
Release:        1%{?dist}
Summary:        Vulkan layer utilizing a small color management / HDR protocol for experimentation

License:        MIT
URL:            https://github.com/swick/VK_hdr_layer
Source:         %{url}/archive/%{commit}.tar.gz

BuildRequires:  meson >= 0.54.0
BuildRequires:  ninja-build
BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(vulkan)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(wayland-scanner)
BuildRequires:  pkgconfig(vkroots)

%description
Hacks for GNOME 47 HDR support. Don't use for serious color work.
Vulkan layer utilizing a small color management / HDR protocol for experimentation. Upstream protocol proposal for color management is here: wp_color_management.

%prep
%autosetup -n VK_hdr_layer-%{commit}

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_datadir}/vulkan/implicit_layer.d/*
%{_libdir}/*.so

%changelog
* Tue Sep 10 2024 Andrey Brusnik <dev@shdwchn.io> - 20240723git.3b5c384-1
- feat: Add vk_hdr_layer package
