%global debug_package %{nil}

Name:           starship
Version:        1.22.1
Release:        1%{?dist}
Summary:        The minimal, blazing-fast, and infinitely customizable prompt for any shell!

License:        ISC
URL:            https://github.com/starship/%{name}
Source:         https://github.com/starship/%{name}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  cargo
BuildRequires:  rust

%description
The minimal, blazing-fast, and infinitely customizable prompt for any shell!
- Fast: it's fast – really really fast! 🚀
- Customizable: configure every aspect of your prompt.
- Universal: works on any shell, on any operating system.
- Intelligent: shows relevant information at a glance.
- Feature rich: support for all your favorite tools.
- Easy: quick to install – start using it in minutes.

%prep
%autosetup -n %{name}-%{version}

%build
cargo build --release --locked

%install
install -Dpm 0755 target/release/%{name} -t %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/%{name}

%changelog
* Tue Jan 14 2025 Andrey Brusnik <dev@shdwchn.io> - 1.22.1-1
- chore(starship): Bump to 1.22.1

* Sat Oct 19 2024 Andrey Brusnik <dev@shdwchn.io> - 1.21.1-1
- chore(starship): Bump to 1.21.1

* Sun Sep 08 2024 Andrey Brusnik <dev@shdwchn.io> - 1.20.1-2
- fix(starship): License

* Sun Aug 04 2024 Andrey Brusnik <dev@shdwchn.io> - 1.20.1-1
- chore(starship): Bump to 1.20.1

* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 1.19.0-2
- refactor: Make RPM specs cleaner

* Sat Jun 08 2024 Andrey Brusnik <dev@shdwchn.io> - 1.19.0-1
- Initial commit
