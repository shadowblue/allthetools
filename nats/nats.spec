%global debug_package %{nil}
%global prjname natscli

Name:           nats
Version:        0.1.6
Release:        1%{?dist}
Summary:        The NATS Command Line Interface

License:        Apache-2.0
URL:            https://github.com/nats-io/%{prjname}
Source:         https://github.com/nats-io/%{prjname}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  go

%description
A command line utility to interact with and manage NATS.

This utility replaces various past tools that were named in the form nats-sub and nats-pub,
adds several new capabilities and support full JetStream management.

Features:
- JetStream management
- Key-Value management
- Object Store management
- Service API management
- JetStream data and configuration backup
- Message publish and subscribe
- Service requests and creation
- Benchmarking and Latency testing
- Deep system account inspection and reporting
- Configuration context maintenance
- NATS eco-system schema registry

%prep
%autosetup -n %{prjname}-%{version}

%build
cd %{name}
go build -ldflags "-w" -o %{name}

mkdir generated_completions
./%{name} --completion-script-bash > generated_completions/%{name}
./%{name} --completion-script-zsh  > generated_completions/_%{name}

%install
install -Dpm 0755 %{name}/%{name} -t %{buildroot}%{_bindir}/
install -Dpm 0644 %{name}/generated_completions/%{name}      -t %{buildroot}/%{bash_completions_dir}/
install -Dpm 0644 %{name}/generated_completions/_%{name}     -t %{buildroot}/%{zsh_completions_dir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{bash_completions_dir}/%{name}
%{zsh_completions_dir}/_%{name}

%changelog
* Wed Dec 25 2024 Andrey Brusnik <dev@shdwchn.io> - 0.1.6-1
- chore(nats): Bump to 0.1.6

* Sat Sep 21 2024 Andrey Brusnik <dev@shdwchn.io> - 0.1.5-1
- feat: Add nats package
