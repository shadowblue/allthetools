%global debug_package %{nil}

Name:           solo2
Version:        0.2.2
Release:        1%{?dist}
Summary:        CLI for the SoloKeys Solo 2 security key

License:        Apache-2.0 OR MIT
URL:            https://github.com/solokeys/%{name}-cli
Source:         https://github.com/solokeys/%{name}-cli/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  cargo
BuildRequires:  rust
BuildRequires:  systemd-devel
BuildRequires:  pcsc-lite-devel

Requires:       systemd-libs
Requires:       pcsc-lite-libs

%description
This tool is experimental and maybe abandoned.
See: https://github.com/solokeys/solo2-cli/discussions/117

%prep
%autosetup -n %{name}-cli-%{version}

%build
cargo build --release --locked

%install
install -Dpm 0755 target/release/%{name} -t %{buildroot}%{_bindir}/
install -Dpm 0644 70-%{name}.rules -t %{buildroot}%{_udevrulesdir}/

%files
%license LICENSE-APACHE LICENSE-MIT
%doc CHANGELOG.md README.md
%{_bindir}/%{name}
%{_udevrulesdir}/70-%{name}.rules

%changelog
* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 0.2.2-1
- feat: Add solo2 package
