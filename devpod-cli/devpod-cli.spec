%global debug_package %{nil}
%global prjname devpod

Name:           devpod-cli
Version:        0.6.13
Release:        1%{?dist}
Summary:        "Codespaces but open-source, client-only and unopinionated."

License:        MPL-2.0
URL:            https://devpod.sh/
Source:         https://github.com/loft-sh/%{prjname}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git
BuildRequires:  go

%description
Codespaces but open-source, client-only and unopinionated:
Works with any IDE and lets you use any cloud, kubernetes or just localhost docker.

%prep
%autosetup -p1 -n %{prjname}-%{version}

%build
go build -ldflags "-w -X github.com/loft-sh/devpod/pkg/version.version=v%{version}" \
                      -o %{prjname}
mkdir generated_completions
./%{prjname} completion bash > generated_completions/%{prjname}
./%{prjname} completion fish > generated_completions/%{prjname}.fish
./%{prjname} completion zsh  > generated_completions/_%{prjname}

%install
install -Dpm 0755 %{prjname} -t %{buildroot}%{_bindir}/
install -Dpm 0644 generated_completions/%{prjname}      -t %{buildroot}%{bash_completions_dir}/
install -Dpm 0644 generated_completions/%{prjname}.fish -t %{buildroot}%{fish_completions_dir}/
install -Dpm 0644 generated_completions/_%{prjname}     -t %{buildroot}%{zsh_completions_dir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/%{prjname}
%{bash_completions_dir}/%{prjname}
%{fish_completions_dir}/%{prjname}.fish
%{zsh_completions_dir}/_%{prjname}

%changelog
* Fri Feb 28 2025 Andrey Brusnik <dev@shdwchn.io> - 0.6.13-1
- chore(devpod-cli): Bump to 0.6.13

* Wed Feb 05 2025 Andrey Brusnik <dev@shdwchn.io> - 0.6.11-1
- chore(devpod-cli): Bump to 0.6.11

* Fri Jan 31 2025 Andrey Brusnik <dev@shdwchn.io> - 0.6.10-2
- fix: enclose in quotes the GPG key identifier in gpg-agent setup
- fix(devpod-cli): write tool version on build

* Thu Jan 30 2025 Andrey Brusnik <dev@shdwchn.io> - 0.6.10-1
- feat: Add devpod-cli package
