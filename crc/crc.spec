%global debug_package %{nil}
%global shortcommit 457b65
%global helpername admin-helper
%global helperver 0.5.4
%global libvirtdrvname machine-driver-libvirt
%global libvirtdrvver 0.13.9

Name:           crc
Version:        2.47.0
Release:        2%{?dist}
Summary:        CRC is a tool to help you run containers

License:        Apache-2.0
URL:            https://github.com/crc-org/%{name}
Source0:        https://github.com/crc-org/%{name}/archive/refs/tags/v%{version}.tar.gz
Source1:        https://github.com/crc-org/%{helpername}/archive/refs/tags/v%{helperver}.tar.gz
Source2:        https://github.com/crc-org/%{libvirtdrvname}/archive/refs/tags/%{libvirtdrvver}.tar.gz

BuildRequires:  git-core
BuildRequires:  go
BuildRequires:  make
BuildRequires:  pkgconfig(libvirt)

Requires:	      libvirt-daemon-kvm
Requires:	      libvirt-client

%description
CRC is a tool to help you run containers.
It manages local VMs to run a OpenShift 4.x cluster,
Microshift or Podman optimized for testing and development purposes

%prep
%setup
tar -xf %{_sourcedir}/v%{helperver}.tar.gz
tar -xf %{_sourcedir}/%{libvirtdrvver}.tar.gz

%build
mkdir embed-files
make -C %{helpername}-%{helperver} VERSION=%{helperver} BINARY_NAME="${PWD}/embed-files/crc-admin-helper-linux-$(go env GOARCH)" build
make -C %{libvirtdrvname}-%{libvirtdrvver} local
mv "%{libvirtdrvname}-%{libvirtdrvver}/crc-driver-libvirt-local" "embed-files/crc-driver-libvirt-$(go env GOARCH)"
make COMMIT_SHA=%{shortcommit} CUSTOM_EMBED=true EMBED_DOWNLOAD_DIR=embed-files/ linux-release-binary embed_crc_helpers

mkdir generated_completions
./out/linux-$(go env GOARCH)/%{name} completion bash > generated_completions/%{name}
./out/linux-$(go env GOARCH)/%{name} completion fish > generated_completions/%{name}.fish
./out/linux-$(go env GOARCH)/%{name} completion zsh  > generated_completions/_%{name}

%install
install -Dpm 0755 ./out/linux-$(go env GOARCH)/%{name} %{buildroot}%{_bindir}/%{name}
install -Dpm 0644 generated_completions/%{name}      -t %{buildroot}/%{bash_completions_dir}/
install -Dpm 0644 generated_completions/%{name}.fish -t %{buildroot}/%{fish_completions_dir}/
install -Dpm 0644 generated_completions/_%{name}     -t %{buildroot}/%{zsh_completions_dir}/

%files
%license LICENSE
%doc README.md
%caps(cap_net_bind_service=eip) %{_bindir}/%{name}
%{bash_completions_dir}/%{name}
%{fish_completions_dir}/%{name}.fish
%{zsh_completions_dir}/_%{name}

%changelog
* Sat Mar 01 2025 Andrey Brusnik <dev@shdwchn.io> - 2.47.0-2
- fix(crc): Set cap_net_bind_service on crc binary

* Sat Mar 01 2025 Andrey Brusnik <dev@shdwchn.io> - 2.47.0-1
- feat: Add crc package
