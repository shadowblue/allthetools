# shadowblue AllTheTools copr repo &nbsp; [![Copr Repo](https://img.shields.io/badge/Copr-Repo-blue)](https://copr.fedorainfracloud.org/coprs/shdwchn10/AllTheTools/)

Repo with different cli tools for [shadowblue](https://gitlab.com/shadowblue) project.

Available packages:

- [chezmoi](https://chezmoi.io)
- [crc](https://crc.dev/)
- [devpod](https://devpod.sh/)
- [eza](https://eza.rocks)
- [fzf](https://github.com/junegunn/fzf)
- [gum](https://github.com/charmbracelet/gum)
- [oc](https://github.com/openshift/oc)
- [onefetch](https://onefetch.dev)
- [nats](https://github.com/nats-io/natscli)
- [nu shell](https://nushell.sh)
- [pueue](https://github.com/Nukesor/pueue)
- [rclone](https://rclone.org/) with enabled ProtonDrive support
- [solo2](https://github.com/solokeys/solo2-cli)
- [sops](https://getsops.io/)
- [starship](https://starship.rs)
- [topgrade](https://github.com/topgrade-rs/topgrade)
- [vk_hdr_layer](https://github.com/swick/VK_hdr_layer)
- [zoxide](https://github.com/ajeetdsouza/zoxide)

### Notes on oc package (OpenShift/OKD CLI)

- Tracks latest branch, when both requirements are met:
  - It's at least oc-x.y.1 patch release in the x.y branch
  - OKD already has release in this branch
- Separate branches also available and they can be installed simultaneously:
  - `oc4.18` (current default for `oc` package)
  - `oc4.17`
  - `oc4.16`
  - `oc4.15`
- You can choose which branch use by default as `/usr/bin/oc` binary with `alternatives(8)` system
- There are also `oc-kubectl`, `oc4.17-kubectl`, etc packages for `kubectl`, which are just symlinks to corresponding `oc` binaries

## Installation

No actions needed for shadowblue OS images. This repo is already enabled for all of them.

Classic Fedora Linux with dnf or RHEL/CentOS Stream/other EL:
```
sudo dnf copr enable shdwchn10/AllTheTools

sudo dnf install <pkgs>
```

Fedora Atomic Desktops/IoT/CoreOS 41+:
```
sudo dnf copr enable shdwchn10/AllTheTools

rpm-ostree install -A <pkgs>
```

Fedora Atomic Desktops/IoT/CoreOS 40 or older:
```
sudo wget -O /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:shdwchn10:AllTheTools.repo https://copr.fedorainfracloud.org/coprs/shdwchn10/AllTheTools/repo/fedora-$(rpm -E %fedora)/shdwchn10-AllTheTools-fedora-$(rpm -E %fedora).repo

rpm-ostree install -A <pkgs>
```

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## License

```
Copyright 2024-2025 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
