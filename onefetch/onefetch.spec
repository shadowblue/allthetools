%global debug_package %{nil}

Name:           onefetch
Version:        2.21.0
Release:        1%{?dist}
Summary:        Command-line Git information tool

License:        MIT
URL:            https://github.com/o2sh/%{name}
Source:         https://github.com/o2sh/%{name}/archive/refs/tags/%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  cargo
BuildRequires:  rust

%description
Onefetch is a command-line Git information tool written in Rust that displays
project information and code statistics for a local Git repository directly
to your terminal. The tool is completely offline - no network access is required.

By default, the repo's information is displayed alongside the dominant language's logo
but you can further configure onefetch to instead use
an image - on supported terminals -, a text input or nothing at all.

It automatically detects open source licenses from texts and provides the user
with valuable information like code distribution, pending changes,
number of dependencies (by package manager), top contributors (by number of commits),
size on disk, creation date, LOC (lines of code), etc.

Onefetch can be configured via command-line flags to display exactly what you want
the way you want it to: you can customize ASCII/Text formatting, disable info lines,
ignore files & directories, output in multiple formats (Json, Yaml), etc.

%prep
%autosetup -n %{name}-%{version}

%build
cargo build --release --locked --features=fail-on-deprecated

%install
install -Dpm 0755 target/release/%{name} -t %{buildroot}%{_bindir}/

%files
%license LICENSE.md
%doc CHANGELOG.md README.md
%{_bindir}/%{name}

%changelog
* Tue Jun 18 2024 Andrey Brusnik <dev@shdwchn.io> - 2.21.0-1
- feat: Added onefetch package
